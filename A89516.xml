<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A89516">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A map of ye kingdome of Ireland. With perticular notes distinguishing the townes reuolted taken or burnt since the late rebellion.</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A89516 of text R210306 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.4[78]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A89516</idno>
    <idno type="STC">Wing M559</idno>
    <idno type="STC">Thomason 669.f.4[78]</idno>
    <idno type="STC">ESTC R210306</idno>
    <idno type="EEBO-CITATION">99869114</idno>
    <idno type="PROQUEST">99869114</idno>
    <idno type="VID">160700</idno>
    <idno type="PROQUESTGOID">2240927006</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A89516)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160700)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f4[78])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A map of ye kingdome of Ireland. With perticular notes distinguishing the townes reuolted taken or burnt since the late rebellion.</title>
     </titleStmt>
     <extent>1 sheet ([1] p.) : map</extent>
     <publicationStmt>
      <publisher>Sould by Will: Webb,</publisher>
      <pubPlace>[Oxford] :</pubPlace>
      <date>[1642?]</date>
     </publicationStmt>
     <notesStmt>
      <note>An engraving 11 1/4x 8 in. This map is placed in the Thomason collection .. between books of March 11 and March 15, 1641/2. .. William Webbe of Oxford is known as a publisher from 1628 to 1639. The Irish rebellion is no doubt that which began on Oct. 23, 1641. -- Madan.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Ireland -- Maps -- Early works to 1800.</term>
     <term>Ireland -- History -- Rebellion of 1641 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A89516</ep:tcp>
    <ep:estc> R210306</ep:estc>
    <ep:stc> (Thomason 669.f.4[78]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>A map of ye kingodme of Ireland. With a perticular notes distinguishing the townes reuolted taken of burnt since the late rebellion.</ep:title>
    <ep:author>anon.</ep:author>
    <ep:publicationYear>1642</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>38</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-08</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-09</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2007-09</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A89516-e10">
  <body xml:id="A89516-e20">
   <pb facs="tcp:160700:1" rend="simple:additions" xml:id="A89516-001-a"/>
   <div type="map" xml:id="A89516-e30">
    <p xml:id="A89516-e40">
     <figure xml:id="A89516-e50">
      <p xml:id="A89516-e60">
       <hi xml:id="A89516-e70">
        <w lemma="a" pos="d" xml:id="A89516-001-a-0010">A</w>
        <w lemma="map" pos="n1" xml:id="A89516-001-a-0020">MAP</w>
        <w lemma="of" pos="acp" xml:id="A89516-001-a-0030">OF</w>
        <w lemma="the" orig="yᵉ" pos="d" xml:id="A89516-001-a-0040">the</w>
        <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A89516-001-a-0060">KINGDOME</w>
        <w lemma="of" pos="acp" xml:id="A89516-001-a-0070">OF</w>
        <w lemma="IRELAND" pos="nn1" xml:id="A89516-001-a-0080">IRELAND</w>
        <pc unit="sentence" xml:id="A89516-001-a-0090">.</pc>
       </hi>
       <w lemma="with" pos="acp" xml:id="A89516-001-a-0100">With</w>
       <w lemma="particular" pos="j" reg="particular" xml:id="A89516-001-a-0110">perticular</w>
       <w lemma="note" pos="n2" xml:id="A89516-001-a-0120">notes</w>
       <w lemma="distinguish" pos="vvg" xml:id="A89516-001-a-0130">distinguishing</w>
       <w lemma="the" pos="d" xml:id="A89516-001-a-0140">the</w>
       <w lemma="town" pos="n2" reg="towns" xml:id="A89516-001-a-0150">Townes</w>
       <w lemma="revolt" pos="vvd" reg="revolted" xml:id="A89516-001-a-0160">reuolted</w>
       <w lemma="take" pos="vvn" xml:id="A89516-001-a-0170">taken</w>
       <w lemma="or" pos="cc" xml:id="A89516-001-a-0180">or</w>
       <w lemma="burn" pos="vvn" xml:id="A89516-001-a-0190">burnt</w>
       <w lemma="since" pos="acp" xml:id="A89516-001-a-0200">since</w>
       <w lemma="the" pos="d" xml:id="A89516-001-a-0210">the</w>
       <w lemma="late" pos="j" xml:id="A89516-001-a-0220">late</w>
       <w lemma="rebellion" pos="n1" xml:id="A89516-001-a-0230">Rebellion</w>
      </p>
      <p xml:id="A89516-e90">
       <w lemma="town" pos="n2" reg="Towns" xml:id="A89516-001-a-0240">Townes</w>
       <w lemma="take" pos="vvn" xml:id="A89516-001-a-0250">taken</w>
       <w lemma="by" pos="acp" xml:id="A89516-001-a-0260">by</w>
       <w lemma="the" pos="d" xml:id="A89516-001-a-0270">the</w>
       <w lemma="rebel" pos="n2" reg="rebels" xml:id="A89516-001-a-0280">Rebells</w>
       <pc xml:id="A89516-001-a-0290">—</pc>
       <gap extent="1" reason="symbol" unit="letter(s)" xml:id="A89516-e100"/>
      </p>
      <p xml:id="A89516-e110">
       <w lemma="town" pos="n2" reg="Towns" xml:id="A89516-001-a-0300">Townes</w>
       <w lemma="besiege" pos="vvn" reg="besieged" xml:id="A89516-001-a-0310">beseiged</w>
       <pc xml:id="A89516-001-a-0320">—</pc>
       <gap extent="1" reason="symbol" unit="letter(s)" xml:id="A89516-e120"/>
      </p>
      <p xml:id="A89516-e130">
       <w lemma="town" pos="n2" reg="Towns" xml:id="A89516-001-a-0330">Townes</w>
       <w lemma="burn" pos="vvn" xml:id="A89516-001-a-0340">burnt</w>
       <pc xml:id="A89516-001-a-0350">—</pc>
       <gap extent="1" reason="symbol" unit="letter(s)" xml:id="A89516-e140"/>
      </p>
      <p xml:id="A89516-e150">
       <w lemma="shall" pos="vmd" reg="Sold" xml:id="A89516-001-a-0360">sould</w>
       <w lemma="by" pos="acp" xml:id="A89516-001-a-0370">by</w>
       <w lemma="Will" pos="nn1" rend="abbr" xml:id="A89516-001-a-0380">Will</w>
       <w lemma="Webb" pos="nn1" xml:id="A89516-001-a-0390">Webb</w>
      </p>
     </figure>
    </p>
   </div>
  </body>
 </text>
</TEI>
